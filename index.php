<!doctype html>
<html>
     <head>
         <?php
         include 'funkcje.php';
         ?>
          <meta charset="UTF-8" />
          <title>To Do It!</title>
          <link rel="stylesheet" type="text/css" href="style.css">
     </head>
     <body>
         <div class="all_box">
          <header id="ap">
              Your list - To Do !
          </header>
          <nav id="left_menu">
              <h2>PROJEKTY</h2>
               <ul>
                    <?php wyswietl_projekt(); ?>
                    <form action="#" method="POST">    
                        <input type="text" name="projekt">
                        <input type="submit" value="+" onClick="window.location.reload();">
                </ul>     
                <?php   dodaj_projekt();?>
                    </form>
                   
          </nav>
          <section id="container">
               <article id="incont">
                    <h2>Projekt <?php $id_projektu= @htmlspecialchars($_GET["p"]); echo $id_projektu; ?> </h2>
                    <hr/>
                    <h2>Do zrobienia:</h2>
                    <ul>
                    <?php wyswietl(); ?>
</ul>
                    <hr/><br>
                    <form action="#" method="POST">
                        <input type="text" name="do_zrb">
                        <input type="submit" Value="+" onClick="window.location.reload();">
                        <?php 
                        to_do(); ?>
                    </form>
               </article> 
          </section>

          <footer>
               
          </footer>
        </div>
     </body>
</html>